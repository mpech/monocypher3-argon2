(de crypto_argon2i (Iters Blocks Pass Salt HashLen)
   (let
      (WorkArea (%@ "malloc" 'P (* 1024 Blocks))
         PL (length Pass)
         SL (length Salt)
         R NIL )
      (native
         "libmonocypher.so.3"
         "crypto_argon2i"
         NIL
         (list 'R (cons HashLen 'B HashLen))
         HashLen
         WorkArea
         Blocks
         Iters
         (cons NIL (cons PL) Pass)
         PL
         (cons NIL (cons SL) Salt)
         SL )
      (prog1
         R
         (%@ "free" NIL WorkArea) ) ) )
