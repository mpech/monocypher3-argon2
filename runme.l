(load "monocypher.l")
(load "libargon2i.l")
(seed (in "/dev/urandom" (rd 8)))
(de randL (A)
   (make (do A (link (rand 0 255)))) )
# min Pass 0, Iters 1, Salt 8, HashLen 4
# (de crypto_argon2i (Iters Blocks Pass Salt HashLen)
# (de libargon2i (Iters Blocks Pass Salt HashLen)
(for HashLen (range 4 8)
   (for Iters (range 1 3)
      (for Blocks (range 8 16)
         (for P (range 1 8)
            (for S (range 8 16)
               (let (Pass (randL P)  Salt (randL S))
                  (test
                     (libargon2i Iters Blocks Pass Salt HashLen)
                     (crypto_argon2i Iters Blocks Pass Salt HashLen) ) ) ) ) ) ) )
(msg 'ok)
(bye)
